-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2018 at 09:25 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `voise`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `i_album` int(11) NOT NULL,
  `i_user` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `year` int(11) NOT NULL DEFAULT '2000',
  `genre` varchar(255) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `i_followers_entry` int(11) NOT NULL,
  `i_follower` int(11) NOT NULL,
  `i_following` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE `playlists` (
  `i_playlist` int(11) NOT NULL,
  `i_user` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `playlist_songs`
--

CREATE TABLE `playlist_songs` (
  `i_playlist_song` int(11) NOT NULL,
  `i_song` int(11) NOT NULL,
  `i_playlist` int(11) NOT NULL,
  `order_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchased_songs`
--

CREATE TABLE `purchased_songs` (
  `i_purchased_song` int(11) NOT NULL,
  `i_song` int(11) NOT NULL,
  `i_user` int(11) NOT NULL,
  `transaction` varchar(255) NOT NULL,
  `purchase_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE `songs` (
  `i_song` int(11) NOT NULL,
  `i_user` int(11) NOT NULL,
  `i_album` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `order_number` int(11) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '0',
  `str_duration` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `i_user` int(11) NOT NULL,
  `password` varchar(200) NOT NULL,
  `access_token` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL DEFAULT 'en',
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) NOT NULL,
  `email_verified` tinyint(4) NOT NULL DEFAULT '0',
  `pass_recovery_key` varchar(100) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `bandname` varchar(255) DEFAULT NULL,
  `eth_wallet` varchar(255) DEFAULT NULL,
  `eth_pk` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `fb_user_id` varchar(255) NOT NULL DEFAULT '',
  `twitter_link` varchar(255) NOT NULL DEFAULT '',
  `facebook_link` varchar(255) NOT NULL DEFAULT '',
  `reddit_link` varchar(255) NOT NULL DEFAULT '',
  `instagram_link` varchar(255) NOT NULL DEFAULT '',
  `youtube_link` varchar(100) NOT NULL,
  `cloud_link` varchar(100) NOT NULL,
  `slack_link` varchar(255) NOT NULL DEFAULT '',
  `telegram_link` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`i_user`, `password`, `access_token`, `language`, `registration_date`, `email`, `email_verified`, `pass_recovery_key`, `firstname`, `lastname`, `bandname`, `eth_wallet`, `eth_pk`, `is_admin`, `deleted`, `fb_user_id`, `twitter_link`, `facebook_link`, `reddit_link`, `instagram_link`, `youtube_link`, `cloud_link`, `slack_link`, `telegram_link`) VALUES
(26, '88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589', '974b5afb6146cd73e0a1624309744176', 'en', '2018-01-24 07:25:16', 'jyothi@gmail.com', 0, NULL, 'jyothi', 'h', NULL, NULL, NULL, 0, 0, '', 'ttwwwwwwwwwwww', 'fffffffffffffffffbbbbbbbbbbbbbb', '', '', '', '', '', ''),
(27, 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', '9cdb2f6c94c0c947919e03c3aa348e27', 'en', '2018-01-24 09:20:03', 'kalyani@gmail.com', 0, NULL, 'jyothi', 'h', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', ''),
(33, 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855k', '6c0e5ec681b1c8ef2d2e9860293d1690', 'en', '2018-01-29 05:45:35', 'jyothibhat@gmail.com', 0, NULL, 'jyothi', 'bhat', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', ''),
(34, 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', '1b2cde4fc1d213097b38cd43d8e8b174', 'en', '2018-01-29 06:26:18', 'jyothihegde@gmail.com', 0, NULL, 'jyothi', 'bhat', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', ''),
(35, '016e2e0233be6174b5c1713c552b565d71b16cad9730e9132c0e016283e23827', 'c155bd2c8d77aa690e111e381ce9d65a', 'en', '2018-01-29 06:40:26', 'jyothihegde@gmil.com', 0, NULL, 'suhail', 'ahmed', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', ''),
(36, '22e6ae454b63268f695b3cad1605001ac303ab5a075bab8d9e8b8975c37d09c8', '5167718d1d932e3137284d7787c2c483', 'en', '2018-01-29 06:47:14', 'poo@gmail.com', 0, NULL, 'poo', 'kk', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', ''),
(37, '0962ef04822f8ca5b341bcbcd783049a454e96b39d23fe9e18200a3edb400bae', '7bb71545afb8be7733953ed64e3fbfe9', 'en', '2018-01-29 06:48:44', 'pooja@gmail.com', 0, NULL, 'poo', 'kk', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', ''),
(38, 'b7b0ed86a6bed33171875dcc53128c2082c788c0d026d6fa73c66b73150913ef', '51d5a7f6041ecdf69e8525095cc2790c', 'en', '2018-01-31 04:57:09', 'jaahnavibhat321@gmail.com', 0, NULL, 'jyothi', 'hegde', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawals`
--

CREATE TABLE `withdrawals` (
  `i_withdrawal` int(11) NOT NULL,
  `i_user` int(11) NOT NULL,
  `transaction` varchar(255) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` double NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL DEFAULT 'VOISE',
  `to_addr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`i_album`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`i_followers_entry`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`i_playlist`);

--
-- Indexes for table `playlist_songs`
--
ALTER TABLE `playlist_songs`
  ADD PRIMARY KEY (`i_playlist_song`);

--
-- Indexes for table `purchased_songs`
--
ALTER TABLE `purchased_songs`
  ADD PRIMARY KEY (`i_purchased_song`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`i_song`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`i_user`);

--
-- Indexes for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD PRIMARY KEY (`i_withdrawal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `i_album` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `i_followers_entry` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `i_playlist` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playlist_songs`
--
ALTER TABLE `playlist_songs`
  MODIFY `i_playlist_song` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchased_songs`
--
ALTER TABLE `purchased_songs`
  MODIFY `i_purchased_song` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `i_song` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `i_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `withdrawals`
--
ALTER TABLE `withdrawals`
  MODIFY `i_withdrawal` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
