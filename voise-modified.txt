//voise_db
//access-token column created in db
ALTER TABLE `users` ADD `access_token` VARCHAR(100) NOT NULL AFTER `password`;


//creating youtube_link column in users table
ALTER TABLE `users` ADD `youtube_link` VARCHAR(100) NOT NULL AFTER `instagram_link`;


//creating cloud_link column in users table
ALTER TABLE `users` ADD `cloud_link` VARCHAR(100) NOT NULL AFTER `youtube_link`;